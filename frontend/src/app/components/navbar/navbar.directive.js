(function() {
  'use strict';

  angular
    .module('wud.techtest')
    .directive('wudNavbar', wudNavbar);

  /** @ngInject */
  function wudNavbar() {
    return {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      controller: ['$scope', '$state', function($scope, $state) {
        $scope.menuItems = [];
        angular.forEach($state.get(), function(obj, key){
          if(obj.hasOwnProperty('menu')){
            $scope.menuItems.push(obj);
          }
        });
      }]
    };
  }
})();
