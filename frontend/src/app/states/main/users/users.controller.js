(function() {
  'use strict';

  angular
    .module('wud.techtest')
    .controller('UsersController', UsersController);

  /** @ngInject */
  function UsersController($log, UserService) {
      var vm = this;

      // object to hold all the data for the user
      vm.userData = {};
      // array og user objects
      vm.userList = [];
      // loader
      vm.loading = true;
      // user notifications
      vm.errorMessage = "";

      // GET all users
      getUsersList();

      // save user
      vm.submitUser = function() {
          vm.loading = true;

          // use the function in service to SAVE
          UserService.save(vm.userData)
              .success(function(data) {
                  // clear form
                  vm.userData = {};

                  // if successful, we'll need to refresh the user list
                  if(data.error){
                      if(data.hasOwnProperty('message'))
                          vm.errorMessage = data.message;
                  }

                  getUsersList();

              })
              .error(function(data) {
                  $log.log(data);
              });
      };

      // DELETE user
      vm.deleteUser = function(id) {
          vm.loading = true;

          // use the function in service to DELETE
          UserService.destroy(id)
              .success(function(data) {
                  $log.log(data);

                  // if successful, we'll need to refresh the user list
                  if(!data.error)
                      getUsersList();

              });
      };

      //    function to get list of all users
      function getUsersList() {
          UserService.get()
              .success(function(data) {
                  //$log.log(data);
                  if(data.error){
                      $log.error('API error');
                  }else {
                      vm.userList = data.urls;
                  }
                  vm.loading = false;
              });
      }

  }
})();
