(function() {
  'use strict';

  angular.module('wud.techtest')
      .factory('UserService', [ '$http', '$httpParamSerializerJQLike', function($http, $httpParamSerializerJQLike) {

        return {
          // get all the users
          get : function() {
            return $http.get('/api/v1/user');
          },

          // save a user (pass in user data)
          save : function(userData) {
            return $http({
              method: 'POST',
              url: '/api/v1/user',
              headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
              data: $httpParamSerializerJQLike(userData)
            });
          },

          // destroy a user
          destroy : function(id) {
            return $http.delete('/api/v1/user/' + id);
          }
        }

      }]);

})();
