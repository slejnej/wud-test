<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function () {
    return View::make('hello');
});

    /**
     * TODO: should probably add routing for 'only' and else redirect to 'index'
     */
Route::group(array('prefix' => 'api/v1'), function () {
    Route::resource('user', 'UserController', array('except' => array('create', 'edit')));
});
