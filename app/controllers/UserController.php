<?php

class UserController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::all();

        return Response::json(array(
            'error' => false,
            'urls' => $users->toArray()
        ),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // check if user exists before adding
        $user = User::where('email', '=', Request::get('email'))->first();

        if($user){
            return Response::json(array(
                'error' => true,
                'message' => "User with this email already exists!"
            ),
                200
            );
        }else {
            $user = new User;
            $user->firstname = Request::get('firstname');
            $user->lastname = Request::get('lastname');
            $user->email = Request::get('email');

            $user->save();

            //XXXXXXXXXXXXXXX Send email to user

            if(App::environment('local')) {
                //  local uses laravel built in SwiftMailer
                Mail::send('emails.welcome', array('firstname' => Request::get('firstname')), function ($message) {
                    $message->from(\Config::get('app.email_from'), 'Laravel');
                    $message->to(Request::get('email'), Request::get('firstname') . ' ' . Request::get('lastname'))->subject('Welcome new user!');
                });
                //  or with Swift_MailTransport as requested
                $transport = Swift_MailTransport::newInstance();

                $message = Swift_Message::newInstance();
                $message->setFrom(\Config::get('app.email_from'), 'Laravel');
                $message->setTo(array(Request::get('email') => Request::get('firstname') . ' ' . Request::get('lastname')));
                $message->setSubject('Welcome new user!');

                //  rendering template
                $view = View::make('emails.welcome', array('firstname' => Request::get('firstname')));
                $message->setBody($view->render());

                $mailer = Swift_Mailer::newInstance($transport);
                $mailer->send($message);
            }else{
                // live uses PHPMailer
                $mail = new PHPMailer();

                $mail->isSMTP();
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );

                $mail->setFrom(\Config::get('app.email_from'), 'Laravel');
                $mail->addAddress(Request::get('email'), Request::get('firstname') . ' ' . Request::get('lastname'));
                $mail->Subject('Welcome new user!');

                //  rendering template
                $view = View::make('emails.welcome', array('firstname' => Request::get('firstname')));
                $mail->msgHTML($view->render());

                $mail->send();

                /**
                 * TODO: create global notification system for reporting if mail was not sent
                 */
            }

            return Response::json(array(
                'error' => false,
                'user' => $user->toArray()
            ),
                200
            );
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return Response::json(array(
            'error' => false,
            'urls' => $user->toArray()
        ),
            200
        );
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $user = User::find($id);
        $user->firstname = Request::get('firstname');
        $user->lastname = Request::get('lastname');
        $user->email = Request::get('email');
        $user->save();

        return Response::json(array(
            'error' => false,
            'urls' => $user->toArray()
        ),
            200
        );
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        /**
         * TODO: should check for relations before removing, because of foreign key check
         */
        $user = User::find($id);
        $user->delete();

        return Response::json(array(
            'error' => false,
        ),
            200
        );
    }
}
