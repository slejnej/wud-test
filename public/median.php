<?php
    /****************************************
     *
     * Project: jslejko.com / median.php
     * Author: Jernej Slejko
     * Date: 13/04/2016
     * Web: http://jslejko.com
     *
     ****************************************/

    $DB_HOST="localhost";
    $DB_DATABASE="techtest";
    $DB_USER="root";
    $DB_PASSWORD="";

    //Connect to mysql server with PDO
    $dsn = "mysql:dbname=$DB_DATABASE;host=$DB_HOST";

    try {
        $db = new PDO($dsn, $DB_USER, $DB_PASSWORD);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
        die();
    }

    $qry1 = "SELECT DISTINCT(year) FROM `user_scores`";
    $years = $db->query($qry1);
?>
    <table border="1">
        <caption>Median is the middle value</caption>

<?php
    while($year = $years->fetch(PDO::FETCH_OBJ)){
        echo "<tr>";
        echo "<td>".$year->year."</td>";

        echo "<td>";
        $qry = 'SELECT CONCAT(u.firstname, " ", u.lastname) fullName,
substring_index
(
  substring_index
  (
    GROUP_CONCAT(`score` ORDER BY `score` DESC), \',\',
    COUNT(*)/2+1
  ),
  \',\', -1
) median
FROM user_scores AS us
INNER JOIN users AS u ON (u.id = us.user_id)
WHERE us.year = "'.$year->year.'"
GROUP BY us.`user_id`
ORDER BY median DESC';
        $users = $db->query($qry);

        while($user = $users->fetch(PDO::FETCH_OBJ)){
            echo $user->fullName." => ".$user->median."<br/>";
        }
        echo "</td>";
        echo "</tr>";
    }

?>
    </table>

<table border="1">
    <caption>Mean is average value</caption>

    <?php
        $years = $db->query($qry1);
        while($year = $years->fetch(PDO::FETCH_OBJ)){
            echo "<tr>";
            echo "<td>".$year->year."</td>";

            echo "<td>";
            $qry = 'SELECT CONCAT(u.firstname, " ", u.lastname) fullName, AVG(score) mean
FROM user_scores AS us
INNER JOIN users AS u ON (u.id = us.user_id)
WHERE us.year = "'.$year->year.'"
GROUP BY us.user_id
ORDER BY mean DESC';
            $users = $db->query($qry);

            while($user = $users->fetch(PDO::FETCH_OBJ)){
                echo $user->fullName." => ".$user->mean."<br/>";
            }
            echo "</td>";
            echo "</tr>";
        }

    ?>
</table>
